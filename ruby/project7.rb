/
	Project Euler - Problem 7
	10001st Prime

/

prime_count = 2

for check_num in 2..300000000

	if prime_count == 10001
		break
	end

	for factor in 2..(Math.sqrt(check_num).floor)

		if check_num % factor == 0
			break
		elsif factor == (Math.sqrt(check_num).floor)
			if prime_count < 10001
				prime_count += 1
			end
			if prime_count == 10001
				puts check_num
				break
			end
		else
			factor += 1
		end
		
	end
	
end