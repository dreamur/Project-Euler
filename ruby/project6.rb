/
	Project Euler - Problem 6
	Sum Square Difference

/

sum_of_squares = 0
square_of_sums = 0

for index in 1..100

	sum_of_squares += (index * index)
	square_of_sums += index
	
end

square_of_sums = square_of_sums ** 2
difference = square_of_sums - sum_of_squares

puts difference