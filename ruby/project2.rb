/
	Project Euler - Problem 2
	Even Fibonacci Numbers
	
/

first = 1
second = 1
third = 0
sum = 0

until (first + second) > 4000000 do
	
	third = (first + second)
	
	if third % 2 == 0
		sum += third
	end
	
	first = second
	second = third
end

puts sum