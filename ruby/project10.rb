/
	Project Euler - Problem 10
	Summation of Primes
	
/

sum = 5
possible_prime = 5

while possible_prime < 2000001

	factor = 3

	while factor < Math.sqrt(possible_prime) + 1
	
		if possible_prime % factor == 0
			break
		elsif factor >= Math.sqrt(possible_prime)
			sum += possible_prime
			break
		else
			factor += 1
		end			
	
	end
	
	possible_prime += 2

end

puts sum