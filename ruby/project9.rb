/
	Project Euler - Problem 9
	Special Pythagorean Triplet

/

upper_bound = 1000

for b in 0...upper_bound
	
	for a in 0...b
	
	
		# didn't have to round with this one
		# using floor or converting to an int
		# causes interpreter to catch a false success
		c = Math.sqrt( ( a * a ) + ( b * b ) )
		
		if a + b + c == upper_bound
		
			puts a, b, c
			puts ( a * b * c )
			upper_bound = 0
			break
		
		end
	
	end

end