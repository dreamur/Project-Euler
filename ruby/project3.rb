/
	Project Euler - Problem 3
	Largest Prime Factor

	
	Keep in mind - A brute force approach
		to a problem of this scale
		simply will not work.
	
	Here we find the prime factorization of
		this number. In doing so, we not only
		find the factors of the number, but
		we also scale the number down (and 
		also the upper bound to the loop) as
		we go by dividing the number by its
		factors. Whenever the leftover number 
		is prime, we can conclude that leftover
		is a factor. We run one last comparison
		on that number to see if it is larger
		than the largest prime we've stored,
		then we display the results.
		
	A better explanation can be found on
		the MathBlog
/

index = 2
largest_prime = 0
target_prime = 600851475143
modified_prime = target_prime

while index * index < modified_prime
	
	if modified_prime % index == 0
		modified_prime /= index
		largest_prime = index
	else
		index = (index == 2) ? 3 : (index + 2)
	end
	
end

if modified_prime > largest_prime
	largest_prime = modified_prime
end

puts largest_prime