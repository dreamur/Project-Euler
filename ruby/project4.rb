/
	Project Euler - Problem 4
	Largest Palindrome Product

/

largest_palindrome = 0

for number_one in 100..999
	for number_two in 100..999
	
		test_number = number_one * number_two
		reverse_number = test_number
	
		if test_number.to_s == reverse_number.to_s.reverse && largest_palindrome < test_number
			largest_palindrome = test_number
		end
	end
end

puts largest_palindrome