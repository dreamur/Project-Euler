/
	Project Euler - Problem 5
	Smallest Multiple

/

upper_bound = 300000000
possible_multiple = 20

while possible_multiple < upper_bound

	factor = 19
	
	# start at 19 - work down to 11 since the 
	# larger numbers are multiples of the smaller
	while factor > 10
	
		# remember modular arithmetic the remainder could've been
		# more than 1, so to cover all of our bases - we'll say that
		# anything that divides unevenly into the possible multiple
		# leaving a remainder of any size will cause the factor check
		# to end
		if possible_multiple % factor > 0
			break
		elsif factor == 11
			upper_bound = 0
			puts possible_multiple
			break
		else
			factor -= 1
		end	
		
	end
	
	possible_multiple += 20

end

/
1 - 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20
2 -   2   4   6   8   10    12    14    16    18    20
3 -     3     6     9       12       15       18    
4 -       4       8         12          16          20
5 -         5         10             15             20
6 -           6             12                18
7 -             7                 14            
8 -               8                     16
9 -                 9                         18
10-                   10                            20

2 - must be even
3 - sum of digits must be divisible by 3
4 - last two digits are divisible by 4 (including 00)
5 - ends in 5 or 0
6 - multiple of 2 and 3
7 - double last digit and subratct from other digits - 7 must divide this number
8 - last three digits are divisible by 8
9 - sum of digits must be divisible by 9
10- last digit is zero

~~~~~~

11 - prime
12 - 6, 4, 3 2 should be skipped already 
13 - prime
14 - skip 7
15 - 5, 3 should be skipped already
16 - skip 8	(4 will be skipped already)
17 - prime
18 - skip 9, 6, 3
19 - prime
20 - skip 10, 5, 4, 2
/